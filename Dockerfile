FROM docker.io/prom/prometheus

LABEL io.containers.autoupdate=registry

COPY ./prometheus.yml /etc/prometheus/prometheus.yml

